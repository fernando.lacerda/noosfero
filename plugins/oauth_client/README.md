README - Oauth Client Plugin
================================

OauthClient is a plugin which allow users to login/signup to noosfero with some oauth providers (for now, google, facebook and noosfero itself).

Install
=======

Enable Plugin
-------------

cd <your_noosfero_dir>
./script/noosfero-plugins enable oauth_client

Active Plugin
-------------

As a Noosfero administrator user, go to administrator panel:

- Click on "Enable/disable plugins" option
- Click on "Oauth Client Plugin" check-box

Provider Settings
=================

Goggle
------

[Create Google+ application](https://developers.google.com/+/web/signin/javascript-flow)

Facebook
--------

[Create Facebook application](https://developers.facebook.com/docs/facebook-login/v2.1)

Github
--------

[Create Github application](https://github.com/settings/developers)

Twitter
--------

- Specially on twitter you need to request user's email address, see more
in https://dev.twitter.com/rest/reference/get/account/verify_credentials

[Create Twitter application](https://apps.twitter.com/)

Shibboleth
--------

This strategy needs Noosfero to be run behind Apache2 webserver with module _shib2_ enabled:

```
sudo apt-get install libapache2-mod-shib2
a2enmod shib2
```

It wasn't tested in different scenarios, but it is possible to use Shibboleth with different web servers. You just need to protect this location, _/plugin/oauth_client/public/callback/shibboleth_, with Shibboleth authentication.

In order to use Shibboleth as an authentication provider, you will need a properly configured Shibboleth Service Provider software in your server. It must be able to process authentication requests from indentity providers and acquire the attributes holding the user's full name and email. 

[Shibboleth Wiki](https://wiki.shibboleth.net/confluence/#all-updates)

#### VIRTUAL HOST CONFIGURATION

The plugin will listen to a callback URL and check for Shibboleth sessions. Once the session is created, the plugin will use the information handed by Shibboleth to build an authentication hash and login/sign up users through it. For that, a set of path settings must be added to Noosfero's virtual host configuration file.

For default production environments, _/etc/noosfero/apache2/virtualhost.conf_:

Overwrite **#HANDLER_URL** with your Shibboleth installation "handlerURL" location defined in _/etc/shibboleth/shibboleth2.xml_ in the "Sessions" tag. In most cases it is "/Shibboleth.sso".

```
# Virtual host configuration for OauthClient Shibboleth strategy.

<Location #HANDLERURL>
require shibboleth
SetHandler shib
</Location>

<Location /plugin/oauth_client/public/callback/shibboleth>
AuthType shibboleth
ShibRequestSetting requireSession 1
require valid-user
ShibUseHeaders On
</Location>
```
#### ATTRIBUTE FIELDS

The Shibboleth strategy settings panel has 2 fields: **Full Name Attribute** and **E-mail Attribute**. They define which of the Shibboleth attributes the plugin will attempt to use when creating new users. The plugin also uses the email to log users in. For example:

```
Full Name Attribute: Shib-inetOrgPerson-cn
E-Mail Attribute: Shib-inetOrgPerson-mail
```

#### REVERSE PROXY CONFIGURATION

If your web server is installed behind a reverse proxy you might encounter the following error when Shibboleth tries to authenticate:

```
opensaml::BindingException at 
(https://example.com/Shibboleth.sso/SAML2/POST)
Invalid HTTP method (GET).
```

If this happens, you should attend to [Shibboleth Service Provider ReverseProxy document](https://wiki.shibboleth.net/confluence/display/SHIB2/SPReverseProxy). You will need to do a few simple modifications to your web server configuration and your Shibboleth SP metadata (Resource and Metadata topics).

Callback
========

This is the callback path that you need to use in your app configuration:

/plugin/oauth_client/public/callback


Varnish Settings
================
If varnish has been used in your stack, you've to bypass the cache for signup page and prevent cookies to be removed when calling the oauth_client plugin callback. E.g.:

```
if (req.url !~ "^/account/*" && req.url !~ "^/plugin/oauth_provider/*" && req.url !~ "^/plugin/oauth_client/*" && req.http.cookie !~ "_noosfero_.*") {
  unset req.http.cookie;
  return(lookup);
}
```

Using Oauth Provider Plugin
===========================
The oauth_provider plugin may be used as a provider in the same noosfero installation that hosts your oauth_client plugin (this is usefull in a multi environment setup).

However, you've to use a distinct set of thin processes to handle the authorization requests (to avoid deadlock).

Apache settings example:
```
RewriteRule ^/oauth_provider/oauth/(authorize|token).*$ balancer://noosfero-oauth-provider%{REQUEST_URI} [P,QSA,L]
```


Development
===========

Running OauthClient tests
--------------------

$ rake test:noosfero_plugins:oauth_client

License
=======

Copyright (c) The Author developers.

See Noosfero license.
