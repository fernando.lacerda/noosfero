class DeserializeFieldsOnOauthClientPluginProvider < ActiveRecord::Migration

  def up
    add_column :oauth_client_plugin_providers, :client_id, :text
    add_column :oauth_client_plugin_providers, :client_secret, :text
    add_column :oauth_client_plugin_providers, :name_field, :text
    add_column :oauth_client_plugin_providers, :email_field, :text
    add_column :oauth_client_plugin_providers, :allow_password_access, :boolean, :default => true
    add_column :oauth_client_plugin_providers, :allow_email_change, :boolean, :default => true

    OauthClientPlugin::Provider.find_each batch_size: 50 do |provider|
      provider.client_id = provider.options.delete :client_id
      provider.client_secret = provider.options.delete :client_secret
      provider.name_field = provider.options.delete :name_field
      provider.email_field = provider.options.delete :email_field
      provider.allow_password_access = provider.options.delete :allow_password_access
      provider.allow_email_change = provider.options.delete :allow_email_change
      provider.save!
    end

    add_index :oauth_client_plugin_providers, :client_id
  end

  def down
    say "this migration can't be reverted"
  end

end
