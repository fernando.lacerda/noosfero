require 'omniauth/strategies/noosfero_oauth2'

class OauthClientPlugin < Noosfero::Plugin

  def self.plugin_name
    "Oauth Client Plugin"
  end

  def self.plugin_description
    _("Login with Oauth.")
  end

  def login_extra_contents
    plugin = self
    proc do
      render :partial => 'auth/oauth_login', :locals => {:providers => environment.oauth_providers.enabled}
    end
  end

  def signup_extra_contents
    plugin = self

    proc do
      if plugin.context.session[:oauth_data].present? then
        if session[:oauth_data][:extra][:login_exists]
          session[:notice] = _("Please select a login name.")
          render :partial => 'account/oauth_existing_login_signup'
        else
          render :partial => 'account/oauth_signup'
        end
      else
        ''
      end
    end
  end

  PROVIDERS = {
    :facebook => {
      :name => 'Facebook',
      :info_fields => 'name,email'
    },
    :google_oauth2 => {
      :name => 'Google'
    },
    :noosfero_oauth2 => {
      :name => 'Noosfero'
    },
    :github => {
      :name => 'Github'
    },
    :twitter => {
      :name => 'Twitter'
    },
    :shibboleth => {
      :name => 'Shibboleth',
      :request_type => :header
    }
  }

  def stylesheet?
    true
  end

  Rails.configuration.to_prepare do
    OmniAuth.config.on_failure = OauthClientPluginPublicController.action(:failure)
  end

  Rails.application.config.middleware.use OmniAuth::Builder do
    PROVIDERS.each do |provider, options|
      setup = lambda { |env|
        request = Rack::Request.new(env)
        strategy = env['omniauth.strategy']

        Noosfero::MultiTenancy.setup!(request.host)
        domain = Domain.by_name(request.host)
        environment = domain.environment rescue Environment.default

        provider_id = request.params['id']
        provider_id ||= request.session['omniauth.params']['id'] if request.session['omniauth.params']
        provider = environment.oauth_providers.find(provider_id)
        strategy.options.merge! consumer_key: provider.client_id, consumer_secret: provider.client_secret
        strategy.options.merge! client_id: provider.client_id, client_secret: provider.client_secret
        strategy.options.merge! options
        strategy.options.merge! provider.options

        if strategy.options.shib_session_id_field? 
          strategy.options.merge! info_fields: {:name => provider.name_field, :email => provider.email_field}
        end

        request.session[:provider_id] = provider_id
      }

      provider provider, :setup => setup,
        :path_prefix => '/plugin/oauth_client',
        :callback_path => "/plugin/oauth_client/public/callback/#{provider}",
        :client_options => { :connection_opts => { :proxy => ENV["OAUTH_HTTP_PROXY"] } }
    end

    unless Rails.env.production?
      provider :developer, :path_prefix => "/plugin/oauth_client", :callback_path => "/plugin/oauth_client/public/callback/developer"
    end
  end

  def account_controller_filters
    check_email_on_oauth_signup_block = proc do 
      auth = session[:oauth_data]
      if auth.present? && params[:user].present?
        params[:user][:oauth_providers] = [OauthClientPlugin::Provider.find(session[:provider_id])]
        if request.post? && auth.info.email != params[:user][:email]
          raise "Wrong email for oauth signup"
        end
      end
    end

    validate_password_access_block = proc do
      user = User.find(session[:user])
      if !user.oauth_providers.first.nil?
        if !user.oauth_providers.first.allow_password_access
          session[:notice] = _("Federated users can't access password functions.")
          redirect_back_or_default(:controller => 'home')
        end
      end
    end

    validate_password_recovery_access_block = proc do
      if request.post?
        @change_password = ChangePassword.new
        requestors = fetch_requestors(params[:value])
        if !requestors.blank? then
          requestors.each do |requestor|
            if !requestor.oauth_providers.first.nil? then
              if !requestor.oauth_providers.first.allow_password_access then
                @change_password.errors.add(:base, _("Federated users can't access password functions."))
                render :action => 'forgot_password'
              end
            end
          end
        end
      end
    end
 
    [{:type => 'before_filter', 
      :method_name => 'check_email_on_oauth_signup',
      :options => {:only => 'signup'},
      :block => check_email_on_oauth_signup_block},
    {:type => 'before_filter',
      :method_name => 'validate_password_access',
      :options => {:only => 'change_password'},
      :block => validate_password_access_block},
    {:type => 'before_filter',
      :method_name => 'validate_password_recovery_access',
      :options => {:only => 'forgot_password'},
      :block => validate_password_recovery_access_block}]
 
   end

   def profile_editor_transaction_extras
     user = User.find(session[:user])
     if user.present? && !user.oauth_providers.first.nil? then
       if !user.oauth_providers.first.allow_email_change && params[:profile_data]['email'] != user.email then
         params[:profile_data]['email'] = user.email
         session[:notice] = _("Federated users can't modify emails.")
       end
     end
   end

  def js_files
    ["script.js"]
  end

end
